package Test;

import com.company.AList;
import com.company.IList;
import com.company.LList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LListTest {

    IList list = new LList();

    @Before
    public void setUp1() {

        list.add(1);
        list.add(2);
        list.add(3);


    }

    @Test
    public void clearTest() {

        list.clear();
        Assert.assertEquals(0, list.size());
    }

    @Test
    public void sizeTest() {

        Assert.assertEquals(3, list.size());
    }

    @Test
    public void getTest() {


        Assert.assertEquals(1, list.get(0));
    }

    @Test
    public void addTest() {

        list.add(4);
        Assert.assertEquals(list.get(3), 4);
    }

    @Test
    public void add2Test() {

        list.add(1, 5);
        System.out.println(list);
        Assert.assertEquals(list.get(1), 5);
    }


    @Test
    public void removeTest() {

        list.remove(1);
        Assert.assertEquals(2, list.size());
    }

    @Test
    public void removeByIndexTest() {

        list.removeByIndex(0);
        Assert.assertEquals(2, list.size());
    }


    @Test
    public void containsTest() {


        Assert.assertTrue(list.contains(1));
    }

    @Test
    public void toArrayTest() {

        int [] mas = new int[]{1,2,3};
        int [] actual = list.toArray();
        Assert.assertEquals(mas[0], actual[0]);
        Assert.assertEquals(mas[1], actual[1]);
        Assert.assertEquals(mas[2], actual[2]);
    }


    @Test
    public void subListTest() {

        AList aList = new AList();
        aList.add(2);
        aList.add(3);
        IList actual =  list.subList(1,2);

        Assert.assertEquals(aList.get(0), actual.get(0));
        Assert.assertEquals(aList.get(1), actual.get(1));

    }

    @Test
    public void removeAllTest() {

        Assert.assertTrue(list.removeAll(new int[]{1}));

    }

    @Test
    public void retainAllTest() {

        Assert.assertTrue(list.retainAll(new int[]{1}));

    }
}
