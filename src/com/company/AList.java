package com.company;

import java.util.Arrays;

public class AList implements IList {

    private int[] arr;
    private int size;
    private final int capacity = 10;
    private int index;



    public AList() {
        this.arr= new int[capacity];
    }

    public AList( int capacity) {
        this.arr= new int[capacity];
    }

    public AList( int[] array) {
        this.arr= array;
    }


    @Override
    public void clear() {
        arr = new int[capacity];
        size =0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public int get(int index) {
        return arr[index];
    }

    @Override
    public boolean add(int number) {

        if (index == arr.length){
            growArray();
        }
        arr[index] = number;
        index++;
        size++;
        return true;
    }


    private void growArray(){
        int[] newArray = new int[(int) Math.ceil((arr.length+1)*1.2)];
        System.arraycopy(arr, 0, newArray, 0, index );
        arr = newArray;
    }

    @Override
    public boolean add(int index, int number) {
        if (index>arr.length || index<0 ){
            throw new ArrayIndexOutOfBoundsException("Wrong Index");
        }
        if (index>size-1 && index<arr.length ){
            add(number);
        }
        int[] newArray = new int[(int) Math.ceil((arr.length+1)*1.2)];

        newArray[index]= number;
        System.arraycopy(arr, index, newArray, index+1,arr.length-index  );
        arr = newArray;
        //1,2,3,4,0,0,0

        return true;
    }

    @Override
    public int remove(int number) {

        for (int i = 0; i <arr.length; i++) {
            if (arr[i]==number){
                removeByIndex(i);
            }
        }

        return 0;
    }

    @Override
    public int removeByIndex(int index) {

        System.arraycopy(arr, index+1, arr, index,arr.length-1-index  );
        size--;
        return 0;
    }

    @Override
    public boolean contains(int number) {
        for (int i = 0; i <size ; i++) {
            if (arr[i]==number)
                return true;
        }
        return false;
    }

    @Override
    public void print() {
        System.out.println(Arrays.toString(toArray()));
    }

    @Override
    public int[] toArray() {
        int []result = new int[size];
        for (int i = 0; i<size; i++){
            result[i]= arr[i];
        }
        return result;
    }

    @Override
    public IList subList(int fromIdex, int toIndex) {
        int k=0;
        if (toIndex>index-1 || toIndex<0){
            throw new ArrayIndexOutOfBoundsException("Wrong Index");
        }
        int [] result = new int[(toIndex-fromIdex)+1];
        for (int i = fromIdex; i <=toIndex ; i++) {
            result[k++]= arr[i];
        }
        return new AList(result);
    }

    @Override
    public boolean removeAll(int[] array) {

        for (int i = 0; i <array.length ; i++) {
            remove(array[i]);
        }
        size=0;
        return true;
    }



    @Override
    public boolean retainAll(int[] array) {
        int []result = new int[arr.length];
        int k=0;
        for (int i = 0; i <arr.length ; i++) {
            for (int j = 0; j < array.length; j++) {
                if (arr[i]==array[j]){
                    result[k++]=arr[i];
                }
            }
        }
        arr = result;
        return true;
    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AList{");
        sb.append("arr=");
        if (arr == null) sb.append("null");
        else {
            sb.append('[');
            for (int i = 0; i < arr.length; ++i)
                sb.append(i == 0 ? "" : ", ").append(arr[i]);
            sb.append(']');
        }
        sb.append('}');
        return sb.toString();
    }
}
