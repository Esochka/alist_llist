package com.company;

public interface IList {

        void clear();
        int size();
        int get(int index);
        boolean add(int number);
        boolean add(int index, int number);
        int remove(int number);
        int removeByIndex(int index);
        boolean contains(int number);
        void print(); //=> выводит в консоль все значения значения в квадратных скобках и через запятую ("[1, 2, 3, 4, 5, 6]")
        int[] toArray();// => приводит данные к массиву, в случае с AList ничего сложного, у нас и так массив
        IList subList(int fromIdex, int toIndex);
        boolean removeAll(int[] array);
        boolean retainAll(int[] array);


}
