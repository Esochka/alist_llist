package com.company;

import java.util.Arrays;

public class LList implements IList {

    private int size;

    class Node {
        int val;
        Node next;

        public Node(int val) {
            this.val = val;
        }

        public void setVal(int val) {
            this.val = val;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Node{");
            sb.append("val=").append(val);
            sb.append(", next=").append(next);
            sb.append('}');
            return sb.toString();
        }
    }

    private Node head;

    public LList() {
    }

    public LList(int[] array) {
        addAll(array);
    }


    @Override
    public void clear() {
        size = 0;
        head = null;
    }

    @Override
    public int size() {

        return this.size;
    }

    @Override
    public int get(int index) {

        if (index < 0)
            throw new RuntimeException("Wrong Index");
        if (index == 0)
            return head.val;
        Node current = null;
        if (head != null) {
            current = head.next;
            for (int i = 0; i < index - 1; i++) {
                if (current.next == null)
                    throw new RuntimeException("Wrong Index");

                current = current.next;
            }
            return current.val;
        }
        throw new RuntimeException("LL Is Empty");
    }

    @Override
    public boolean add(int number) {

        if (head == null) {
            head = new Node(number);
            size++;
            return true;

        } else {

            Node tmp = new Node(number);
            Node current = head;


            if (current != null) {

                while (current.next != null) {
                    current = current.next;
                }

                current.setNext(tmp);

            }
            size++;

            return true;
        }

    }

    public void addAll(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            add(arr[i]);
        }
    }

    @Override
    public boolean add(int index, int number) {
        Node tmp = new Node(number);
        Node current = head;


        if (current != null) {

            for (int i = 0; i < index - 1 && current.next != null; i++) {
                current = current.next;
            }
        }

        tmp.setNext(current.next);

        current.setNext(tmp);

        size++;
        return true;
    }

    @Override
    public int remove(int number) {
        int index = 0;
        Node current = head;
        while (current.val != number) {
            current = current.next;
            index++;

        }
        removeByIndex(index);


        return 0;
    }

    @Override
    public int removeByIndex(int index) {
        Node current = head;
        if (index < 0 || index > size())
            throw new RuntimeException("Wrong Index");
        if (index == 0) {
            head = head.next;
            size--;
            return 0;
        }


        if (head != null) {
            for (int i = 0; i < index - 1; i++) {
                if (current.next == null)
                    throw new RuntimeException("Wrong Index");

                current = current.next;
            }
            current.setNext(current.next.next);
            size--;

        }

        return 0;
    }

    @Override
    public boolean contains(int number) {

        Node current = head;
        if (current.val == number) {
            return true;
        }
        while (current.next != null) {

            current = current.next;
            if (current.val == number) {
                return true;
            }


        }
        return false;
    }

    @Override
    public void print() {
        try {
            System.out.println(Arrays.toString(toArray()));
        } catch (NullPointerException e) {
            System.out.println("LL Is Empty");
        }


    }

    @Override
    public int[] toArray() {
        int[] array = new int[size];
        int k = 1;
        Node current = head;
        array[0] = current.val;
        while (current.next != null) {


            current = current.next;
            array[k++] = current.val;

        }

        return array;
    }

    @Override
    public IList subList(int fromIdex, int toIndex) {
        int[] array = toArray();
        int k = 0;
        if (toIndex > size - 1 || toIndex < 0) {
            throw new ArrayIndexOutOfBoundsException("Wrong Index");
        }
        int[] result = new int[(toIndex - fromIdex) + 1];
        for (int i = fromIdex; i <= toIndex; i++) {
            result[k++] = array[i];
        }
        return new LList(result);
    }

    @Override
    public boolean removeAll(int[] array) {
        for (int i = 0; i < array.length; i++) {
            remove(array[i]);
        }

        return true;
    }

    public boolean addStart(int number) {

        Node newNode = new Node(number);

        if (head == null) {
            head = newNode;

        } else {

            Node temp = head;
            head = newNode;
            head.next = temp;
        }
        size++;


        return true;
    }

    public boolean addEnd(int number) {

        return add(number);
    }


    @Override
    public boolean retainAll(int[] array) {
        int[] arra = toArray();
        int cont = 0;
        for (int i = 0; i < arra.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (arra[i] == array[j]) {
                    cont++;
                }
            }
        }
        int[] result = new int[cont];

        int k = 0;
        for (int i = 0; i < arra.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (arra[i] == array[j]) {
                    result[k++] = arra[i];

                }

            }
        }
        arra = result;

        clear();

        addAll(arra);

        return true;
    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("LList{");
        sb.append("head=").append(head);
        sb.append('}');
        return sb.toString();
    }
}
